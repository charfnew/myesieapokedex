package com.example.myapplication;

import java.util.ArrayList;

public class PokemonResponse {

    private ArrayList<PokeURL> results;

    public ArrayList<PokeURL> getResults() {
        return results;
    }

    public void setResults(ArrayList<PokeURL> results) {
        this.results = results;
    }
}