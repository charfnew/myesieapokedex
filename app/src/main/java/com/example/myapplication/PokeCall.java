package com.example.myapplication;

import androidx.annotation.NonNull;

import com.google.gson.JsonObject;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PokeCall {

    @GET("pokemon")
    Call<PokemonResponse> getListofPkmn(@Query("limit") int limit);

    @GET("pokemon/{id}")
    Call<PokemonIssue> getPokemon(@Path("id") int id);
}
