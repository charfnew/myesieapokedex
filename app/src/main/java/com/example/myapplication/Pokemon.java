package com.example.myapplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;

import java.net.URL;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Pokemon extends RealmObject {
    @PrimaryKey
    public long id;
    public String sprite;
    public String name;
    /*public String type1;
    public String type2;*/
    public int poid;
    public int taille;

    public Pokemon(){}

   public Bitmap getSprite(String sprite){
        URL url;
        try{
           url = new URL(sprite);
            Bitmap bpm = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            return bpm;} catch (Exception e){}
        return null;
    }

    public void setSprite(String sprite){this.sprite = sprite;}

    public long getId(){return id;}

    public void setId(long id){this.id = id;}

    public String getName(){return name;}

    public void setName(String name){this.name = name;}

    /*public String getType1(){return type1;}

    public void setType1(String type1){this.type1 = type1;}

    public String getType2(){return type2;}

    public void setType2(String type2){this.type2 = type2;}*/

    public int getPoid(){return poid;}

    public void setPoid(int poid){this.poid = poid;}

    public int getTaille(){return taille;}

    public void setTaille(int taille){this.taille = taille;}

    public void setParameters(String sprite, String name, int poid, int taille){
        setSprite(sprite);
        setName(name);
        setPoid(poid);
        setTaille(taille);
        /*setType1(type1);
        setType2(type2);*/
    }
}
