package com.example.myapplication;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class RealmController{

    private RealmController(Application application){
        realm = Realm.getDefaultInstance();
    }

    private static RealmController instance;
    private final Realm realm;
    private RealmResults<Pokemon> fasRealmResults;

    public static RealmController getInstance(Application application){
        if(instance==null){
            instance= new RealmController(application);
        }
        return instance;
    }


    public void addPokemon(long id, String sprite,String name,int poid,int taille){
        realm.beginTransaction();
        Pokemon pokemon = realm.createObject(Pokemon.class,id);
        pokemon.setParameters(sprite, name, poid,  taille);
        realm.commitTransaction();
    }

    public void deletAllStoredPokemon(){
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
    }

    public RealmResults<Pokemon> getStoredPokemon(){
        fasRealmResults = realm.where(Pokemon.class)
                .findAll();
        return fasRealmResults;
    }

}