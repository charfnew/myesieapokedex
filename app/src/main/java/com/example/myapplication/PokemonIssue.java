package com.example.myapplication;

import java.util.ArrayList;
import java.util.Map;

public class PokemonIssue {

    private int height;
    private int id;
    private String name;
    //private ArrayList<Object> type;
    private int weight;


    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*public ArrayList<Object> getType() {
        return type;
    }

    public void setType(ArrayList<Object> type) {
        this.type = type;
    }*/

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
