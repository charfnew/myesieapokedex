package com.example.myapplication;

public class PokeURL {
    private int id;
    private String name;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        String[] urlSegments = url.split("/");
        return Integer.parseInt(urlSegments[urlSegments.length-1]);
    }

    public void setId(int id) {
        this.id = id;
    }
}
