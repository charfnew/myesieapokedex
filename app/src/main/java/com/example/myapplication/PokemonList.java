package com.example.myapplication;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.RealmController;

import java.util.ArrayList;

public class PokemonList extends AppCompatActivity {

    RealmController realm= RealmController.getInstance(this.getApplication());
    String url;
    private RealmResults<Pokemon> pokemonRealmResults;
    private RecyclerView pokemlist;
    private PokemonAdapter pokemonAdapter;
    private static final String TAG = "POKEDEX";
    private static final String[] REQUIRED_PERMISSION = new String[]{Manifest.permission.INTERNET};
    public final static int REQUEST_CODE_PERMISSIONS = 1;

    private PokeRepository pokeRepository;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_list);
        this.pokeRepository = pokeRepository.getInstance();
        //getPkmnList();
        if(allPermissionsGranted()){
            getPkmnList();
        }
        realm.deletAllStoredPokemon();

        pokemlist = findViewById(R.id.pokeDisplay);
        pokemonRealmResults = RealmController.getInstance(this.getApplication()).getStoredPokemon();
        pokemonAdapter = new PokemonAdapter(pokemonRealmResults, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        pokemlist.setLayoutManager(linearLayoutManager);
        pokemlist.setAdapter(pokemonAdapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        Log.e(TAG, "entering onRequestPermissionsResult");
        if( requestCode == REQUEST_CODE_PERMISSIONS) {
            if(allPermissionsGranted()) {
                return;
            } else {
                Toast.makeText(this, "Permissions not granted by the user", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private boolean allPermissionsGranted() {
        for(String permission : REQUIRED_PERMISSION) {
            if(ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public void getPkmnList(){
       int limit = 151;
        pokeRepository.getListofPkmn(limit)
                .enqueue(new Callback<PokemonResponse>() {
                    @Override
                    public void onResponse(Call<PokemonResponse> call, Response<PokemonResponse> response) {
                        PokemonResponse pokemonResponse = response.body();
                        ArrayList<PokeURL> p = pokemonResponse.getResults();
                        int s = 0;
                        for(int i = 0; i < 150; i++) {
                            s = Integer.parseInt(p.get(i).getUrl().split("/")[p.get(i).getUrl().split("/").length-1]);
                            pokeRepository.getPokemon(s).enqueue(new Callback<PokemonIssue>() {
                                @Override
                                public void onResponse(Call<PokemonIssue> call, Response<PokemonIssue> response2) {
                                    PokemonIssue pute = response2.body();
                                    String url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + pute.getId() + ".png";
                                    realm.addPokemon(pute.getId(),url, pute.getName(),pute.getWeight(),pute.getHeight() );
                                    Log.i("POKEMERDE", pute.getName());
                                }

                                @Override
                                public void onFailure(Call<PokemonIssue> call, Throwable t2) {
                                    Log.e("POKEMERDE", t2.getMessage());
                                }
                            });

                            System.out.println(s);
                        }
                    }

                    @Override
                    public void onFailure(Call<PokemonResponse> call, Throwable t) {

                    }
                });
        // pokeRepository.getListofPkmn(limit);
    }


}