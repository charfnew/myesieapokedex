package com.example.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.net.URL;

import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

public class PokemonAdapter extends RealmRecyclerViewAdapter <Pokemon, PokemonAdapter.PokemonViewHolder> {
    Context c;
    private RealmResults<Pokemon> pokemons;
    private URL url;
    private Bitmap bpm;

    public static class PokemonViewHolder extends RecyclerView.ViewHolder{
        public TextView nomPkmn, taillePkmn, poidPkmn, type1, type2;
        public ImageView sprite;

        public PokemonViewHolder(View pokemon){
            super(pokemon);
            sprite = pokemon.findViewById(R.id.sprite);
            nomPkmn = pokemon.findViewById(R.id.name);
            type1 = pokemon.findViewById(R.id.type1);
            type2= pokemon.findViewById(R.id.type2);
            poidPkmn = pokemon.findViewById(R.id.poid);
            taillePkmn = pokemon.findViewById(R.id.taille);
        }
    }

    public PokemonAdapter(RealmResults<Pokemon> pokemons, Context context){
        super(pokemons,true,true);
        this.pokemons = pokemons;
        this.c= context;

    }

    @NonNull
    @Override
    public PokemonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View pokemon = LayoutInflater.from(parent.getContext()).inflate(R.layout.pokemon, parent, false);
        PokemonViewHolder pokemonViewHolder = new PokemonViewHolder(pokemon);
        return pokemonViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PokemonViewHolder pokemonViewHolder, int position){
        Pokemon pokemon = pokemons.get(position);
        pokemonViewHolder.sprite.setImageBitmap(pokemon.getSprite(pokemon.sprite));
        pokemonViewHolder.nomPkmn.append(pokemon.getName());
        /*pokemonViewHolder.type1.append(pokemon.getType1());
        pokemonViewHolder.type2.append(pokemon.getType2());*/
        pokemonViewHolder.poidPkmn.append(pokemon.getPoid() + "");
        pokemonViewHolder.taillePkmn.append(pokemon.getTaille() +"");
    }
}
