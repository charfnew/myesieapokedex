package com.example.myapplication;

import com.google.gson.JsonObject;

import java.util.ArrayList;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class PokeRepository {

    private static volatile PokeRepository instance;

    private PokeCall pokeCall;

    private PokeRepository() {
        creatPokeCall();
    }

    private void creatPokeCall() {
        this.pokeCall = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build().create(PokeCall.class);
    }

    public static PokeRepository getInstance() {
        if (instance == null) {
            instance = new PokeRepository();
        }
        return instance;
    }

    public Call<PokemonResponse> getListofPkmn(int limit){
        return this.pokeCall.getListofPkmn(limit);
    }

    public Call<PokemonIssue> getPokemon(int id){
        return this.pokeCall.getPokemon(id);
    }

}
