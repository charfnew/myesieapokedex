package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Context;
import android.content.Intent;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.internal.DebouncingOnClickListener;
import io.realm.Realm;
import io.realm.RealmConfiguration;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.example.myapplication.PokemonList;

import java.util.function.Consumer;


public class MainActivity extends AppCompatActivity {

    @BindView(R.id.enter)
    AppCompatButton button;

    @OnClick(R.id.enter)
    void startPokedex(){
        Intent intent = new Intent(this,PokemonList.class);
        this.startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initRealm();
        ButterKnife.bind(this);
    }

    private void initRealm(){
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("tasky.realm")
                .schemaVersion(0)
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }


}